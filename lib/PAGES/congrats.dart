import 'package:flutter/material.dart';

class Congrats extends StatefulWidget {
  @override
  _CongratsState createState() => _CongratsState();
}

class _CongratsState extends State<Congrats> {
  /*void initState() {
    Timer(Duration(seconds: 2), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => SiginInUp()));
    });
    super.initState();
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * .15),
          Container(
            height: MediaQuery.of(context).size.height * .4,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  'assets/accept.png',
                ),
              ),
            ),
          ), //accept_img container
          SizedBox(
            height: MediaQuery.of(context).size.height * .05,
          ),
          Center(
            child: Container(
              height: 75,
              width: 75,
              child: Image.asset(
                'assets/ok logo.png',
              ),
            ),
          ), //ok_img container

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Congrats! \nYou'll have an account now!",
                textAlign: TextAlign.center,
                style: TextStyle(
                  height: 1.5,
                  fontSize: 25.0,
                  color: Color(0xff40A3A3),
                ), //TextStyle
              ), //text
            ],
          ), //row
        ],
      ),
    ); //Scaffold
  }
}// Congrats class
