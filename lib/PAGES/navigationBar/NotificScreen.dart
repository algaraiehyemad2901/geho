import 'package:flutter/material.dart';
import './notiPage1.dart';

class NotificScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: Text('Open route'),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => NotiPage1()));
          },
        ),
      ),
    );
  }
}
